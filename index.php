<?php 

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="assets/semantic.min.css">
    <script src="assets/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="assets/semantic.min.js" type="text/javascript"></script>
    <script src="assets/jquery.form.js" type="text/javascript"></script>
    <script src="assets/myScript.js">

        
    </script>
    <style>

        .spread{
            width: 100%;

        }
        @media (max-width: 767px) {

            .spread{
                height: 65px;
            }

        }
        .ui.button.spread{
            font-size: 190%;
            font-weight: 300;

        }
        .ui.huge.header{
            font-size:250%;
            font-weight: 400;
        }

    </style>
</head>
<body>
    <section>
        <div style='background-image: url("assets/beach.jpg");height: 640px;margin-top: -40px;'>
            <div style="width: 100%;height: 100%;background-color: rgba(255,255,255,0.25);">
                <br><br><br><br>
                <div class="">
                    <h1 class="ui huge header centered" style="color: #dddeeb;">WardWiz DB uploader</h1>
                </div><br>

                <div class="ui grid">

                    <div class="three mobile sixteen wide tablet three wide computer column">

                    </div>
                    <div class="five mobile sixteen wide tablet five wide computer column center">
                        <a class="ui button blue spread" id="firstLogin">
                            Update Database
                        </a>
                    </div>
                    <div class="five mobile sixteen wide tablet five wide computer column">
                        <a class="ui button blue spread" onclick='$("#mailModal").modal("show")'>
                            Email all
                        </a>
                    </div>

                    <div class="three mobile sixteen wide tablet three wide computer column">

                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="ui modal" id="mailModal">
        <div class="header">Insert data to mail</div>

        <div class="content">
            <div class="response"></div>
            <form id="mailForm" autocomplete="off" method="POST" action="mail/sendMail.php">

                <div class="ui form">
                    
                    <div class="field">
                        <label>String Based Count</label>
                        <input type="number" placeholder="String based count" name="string">
                    </div>
                    <div class="field">
                        <label>Hash Based Count</label>
                        <input type="number" placeholder="Hash based count" name="hash">
                    </div>
                    <div class="field">
                        <label>DB version</label>
                        <input type="text" placeholder="DB Version" name="version">
                    </div>
                    <input type="hidden" name="sendMail" value="true">
                </div>
            </form>
            <div class="ui mainLoader" style="height: 130px;display:none;">
                <div class="ui active dimmer">
                    <div class="ui text huge loader">Sending Mail<br>Please wait!</div>
                </div>
                <p></p>
            </div>
        </div>
        <div class="actions">
            <div class="ui black deny button">
                Close
            </div>
            <div class="ui green button" id="mailButton">
                Send Mail
            </div>
        </div>

    </div>
    <div class="ui modal" id="loginModal">
        <i class="close icon"></i>
        <div class="header">Login to Upload</div>

        <div class="content">
            <form id="loginForm" autocomplete="off">

                <div class="ui form">
                    <div class="field">
                        <label>Username</label>
                        <input type="text" placeholder="Username" name="user">
                    </div>
                    <div class="field">
                        <label>Password</label>
                        <input type="password" placeholder="password" name="password">
                    </div>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui black deny button">
                Cancel
            </div>
            <div class="ui positive right labeled icon button" id="loginButton">
                Proceed
                <i class="checkmark icon"></i>
            </div>
        </div>

    </div>
    <div class="ui modal" id="uploadModal">
        <div class="header">Upload file</div>

        <div class="content">
            <form id="uploadForm" enctype="multipart/form-data" method="POST" action="../db_upload_test.php">
                <div class="ui form">
                    <div class="field">
                        <input type="file" name="zip" class="ui button">
                        <input type="hidden" name="submit" value="true">
                    </div>
                    
                </div>
            </form>
            <br>
            <div class="ui indicating progress" data-percent="0" id="uploadProgress" style="display:none;">
                <div class="bar"></div>
                <div class="label labelpercent"><span class="percent">0</span>% Uploaded</div>
                
            </div>
            <div class="ui mainLoader" style="height: 130px;display:none;">
                <div class="ui active dimmer">
                    <div class="ui text huge loader">Preparing Files<br>Please wait!</div>
                </div>
                <p></p>
            </div>
            <div class="ui segment labelResponse" style="display:none;height: 200px;overflow: auto;"></div>
        </div>
        
        <div class="actions">
            <div class="ui black deny button">
                Close
            </div>
            <div class="ui green button" id="uploadButton">
                Upload
            </div>
        </div>

    </div>
</body>
</html>