<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if(isset($_POST['sendMail'])){
    $strBased = $_POST['string'];
    $hashBased = $_POST['hash'];
    $version = $_POST['version'];
    
    if(empty($strBased)){
        echo json_encode(array('status' => 0,'message' => "Please enter string based count"));
        exit;
    }
    elseif(empty($hashBased)){
        echo json_encode(array('status' => 0,'message' => "Please enter hash based count"));
        exit;
    }
    elseif(empty($version)){
        echo json_encode(array('status' => 0,'message' => "Please enter DB version"));
        exit;
    }
    elseif($strBased && $hashBased && $version){
        $output = shell_exec("python testingmail.py $strBased $hashBased $version");
        if(strpos($output, "Mail Sent")){
            echo json_encode(array('status' => 1,'message' => $output));
        }
        else{
            echo json_encode(array('status' => 0,'message' => $output));
        }
        
        exit;
    }
    
}

?>

