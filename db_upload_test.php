
<?php
if(isset($_POST['submit'])){
    $zip = new ZipArchive();
    copy($_FILES['zip']['tmp_name'], "new_db_temp.zip");
    if($zip->open("new_db_temp.zip") === true){
        $filename = $_FILES['zip']['name'];
        if(!file_exists("{$filename}/"))
            mkdir("{$filename}/");
        $zip->extractTo("{$filename}/");

        if(file_exists("{$filename}/Online_New")){
            $backuped_dir = "WardWizUpdatesTesting".date("d_m_Y_h_i_s");
            mkdir($backuped_dir);
            echo "<br>Backup directory created: $backuped_dir<br>";
            system("xcopy WardWizUpdatesTesting $backuped_dir /e/s/y/q");
            echo "<br><br>Backup successfull to directory: $backuped_dir";
            //var_dump($outpt);
            //copy("stuff/WardWizUpdatesTesting", "stuff/WardWizUpdatesTesting".date("d_m_Y"));

            if ($dh = opendir($filename."/Online_New/CommonDB")){
                while (($file = readdir($dh)) !== false){
                    if($file != "." && $file != ".."){
                        echo "<br><br>Copying file:" . $file ;
                        copy("".$filename."/Online_New/CommonDB/".$file, "WardWizUpdatesTesting/Basic/CommonDB/$file");
                        echo "<br>Copied successfully: ".$filename."/Online_New/CommonDB/".$file." ==>> WardWizUpdatesTesting/Basic/CommonDB/$file";
                        copy("".$filename."/Online_New/CommonDB/".$file, "WardWizUpdatesTesting/Essential/CommonDB/$file");
                        echo "<br>Copied successfully: ".$filename."/Online_New/CommonDB/".$file." ==>> WardWizUpdatesTesting/Essential/CommonDB/$file";
                        copy("".$filename."/Online_New/CommonDB/".$file, "WardWizUpdatesTesting/CommonDB/$file");
                        echo "<br>Copied successfully: ".$filename."/Online_New/CommonDB/".$file." ==>> WardWizUpdatesTesting/CommonDB/$file";
                    }

                }
                closedir($dh);
            }
            if ($dh = opendir("".$filename."/Online_New")){
                while (($file = readdir($dh)) !== false){
                    if($file != "." && $file != ".." && $file != "CommonDB" && $file != "WardWizMalStatistics.txt"){
                        echo "<br><br>Copying ini:" . $file ;
                        copy("".$filename."/Online_New/".$file, "WardWizUpdatesTesting/$file");
                        echo "<br>Copied successfully: ".$filename."/Online_New/".$file." ==>> WardWizUpdatesTesting/$file";
                    }

                }
                closedir($dh);
            }
            //echo "rmdir /s /q stuff\{$filename}";
            system("rmdir /s/q ".$filename);
            system("rm -rf new_db_temp.zip");
        }
        echo "<br>Task Done Successfully";
        $zip->close();
    }
}
?>
    