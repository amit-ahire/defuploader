/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
            $('.ui.button.blue').hover(
                function(){ $(this).addClass('basic') },
                function(){ $(this).removeClass('basic') }
            );

            $("#firstLogin").click(function(){
                $("#loginForm .ui.error.message").remove();
                $("#loginModal").modal("show");
            });

            $("#loginButton").click(function(){
                $.ajax ({
                    url: 'validate.php',
                    type: 'POST',
                    data: $("#loginForm").serialize()+'&login=true',
                    success: function(data){
                        var result = JSON.parse(data);

                        if(result.status == 1){
                            $("#loginForm .ui.error.message").remove();
                            $("#uploadModal").modal('setting', 'closable', false).modal("show");
                        }
                        else{
                            $("#loginForm .ui.error.message").remove();
                            $("#loginForm").prepend('<div class="ui error message">!Login Failed due to: '+result.message+'</div>');
                            $("#loginModal").modal("show");
                        }
                    }

                });
            });
            $("#uploadButton").click(function(){
                $("#uploadForm").submit();
            });
            $("#mailButton").click(function(){
                $("#mailForm").submit();
            });
            var percent = $(".percent");
            $("#uploadForm").ajaxForm({
                beforeSend: function() {
                    $("#uploadProgress").show();
                    $("#uploadProgress").progress({percent:0});
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete;
                    
                    $("#uploadProgress").progress({percent:percentComplete});
                    percent.html(percentVal);
                    if(percentVal == 100){
                        $("#uploadModal .mainLoader").show();
                        $("#uploadModal").css("margin-top", (parseInt($("#uploadModal").css("margin-top")) - 50)+"px");
                    }
                },
                complete: function(xhr) {
                    $("#uploadModal .mainLoader").hide();
                    $("#uploadProgress").hide();
                    $("#uploadModal").css("margin-top", (parseInt($("#uploadModal").css("margin-top")) - 50)+"px");
                    $(".labelResponse").before('<div class="ui ignored positive message">Update Successfull.</div>');
                    $(".labelResponse").html(xhr.responseText);
                    $(".labelResponse").show();
                    
                }
            });
            $("#mailForm").ajaxForm({
                beforeSend: function() {
                    $("#mailModal .mainLoader").show();
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete;
                    
                    if(percentVal == 100){
                        $("#mailModal .mainLoader").show();
                        $("#uploadModal").css("margin-top", (parseInt($("#uploadModal").css("margin-top")) - 50)+"px");
                    }
                },
                complete: function(xhr) {
                    var result = JSON.parse(xhr.responseText);
                    if(result.status == 1){
                        $("#mailModal .response").html('<div class="ui ignored positive message">'+result.message+'.</div><br>');
                    }
                    else{
                        $("#mailModal .response").html('<div class="ui ignored error message">'+result.message+'.</div><br>');
                    }
                    $("#mailModal .mainLoader").hide();
//                    $("#uploadProgress").hide();
//                    $("#uploadModal").css("margin-top", (parseInt($("#uploadModal").css("margin-top")) - 50)+"px");
//                    $(".labelResponse").before('<div class="ui ignored positive message">Update Successfull.</div>');
//                    $(".labelResponse").html(xhr.responseText);
//                    $(".labelResponse").show();
                    
                }
            });

        });



